from wsconnect import ClientClosedError
from wsserver import WebSocketServer, WebSocketClient
from servo import doCmd


class zClient(WebSocketClient):
  def __init__(self, conn):
    super().__init__(conn)

  def process(self):
    try:
      if self.connection.is_closed(): return
      msg = self.connection.read()
      if not msg: return
      msg = msg.decode("utf-8")
      items = msg.split(" ")
      cmd = items[0]
      response = doCmd(cmd)
      self.connection.write(response)
    except ClientClosedError:
      print("Connection error")
      self.connection.close()


class zServer(WebSocketServer):
  def __init__(self):
    super().__init__("servo.html", 2)

  def _make_client(self, conn):
    return zClient(conn)


def w():
  server = zServer()
  server.start()
  try:
    while True:
      server.process_all()
  except KeyboardInterrupt: pass
  server.stop()

w()
