# Micropython Open Hardware Robot Arm

This repository describes how you can control a simple four degrees of freedom
robot arm using MicroPython on a board with a esp8266 or esp32.
The esp will accomplish the following services:
* Access point on IP address 192.168.4.1
* giving access to up to four devices in its neighborhood
* On its ip address it will serve an html page with buttons to control the arm
* It will control the individual motors of the arm using one pin for every motor via PWM

In short: you will be able to control the arm from your smart phone over wifi
without the need for any other external infrastructure. 

## Assembly and 3d printed parts
This software controls a toy robot arm with only four degrees of freedom driven
by four (mini) servo motors. I got the design of the arm from
([eezyrobots.it/mk1](http://www.eezyrobots.it/eba_mk1.html)),
but you may want to make the more sturdy version at
([eezyrobots.it/mk2](http://www.eezyrobots.it/eba_mk2.html)).

## Needed parts
You can get all necesary hardware for this arm for less than
* 3d printed parts as detailed in the links above: 3€
* m4 screws and nuts: 2€
* 4 mini servo motors: 4€
* esp8266 or esp32 developer board with usb: 4€
* power supply 5 or 6 volt: 3€

So the hardware will cost about 16€ if you buy it directly in China, for example
with [AliExpress](https://www.AliExpress.com).

## Needed skills
* You should have some experience with a solder iron but nothing fancy
* You need to find a way to connect ground and 5 or 6 volt directly to all servos
* You need to download and install some software on your computer

## Howto use

### One time preparation on your computer
* Download the latest MicroPython build for esp8266 or esp32 from [docs.micropython.org](https://docs.micropython.org).
* Install python3 on your computer
* Optionally (preferrably) make a (python3) virtualenv
* pip install esptool
* pip install ampy
* Install minicom on your computer or alternatively install arduino on your computer. (If you are om Windows installing Arduino will also install the needed USB drivers)

### One time preparation to get started on your esp board
* Attach the esp board to your computer using a usb cable
* find out what device name you need to use, on Linus, most probably it would be /dev/ttyUSBx with x=0 for example
* Upload the html and py files using "ampy put <file>" (See "ampy --help")

### Manually controlling the robot arm
* Attach the esp board to your computer
* Connect to your board with "minicom -D /dev/ttyUSB0 -b 115200"
* Now you can stop the webserver that is running on your board by typing a <C-C> twice and get your Python prompt (>>> ).