from machine import Pin, PWM
from time import sleep
import json

def run(): pass
def mid(): pass

points = [ # these positions are visited when run is executed
# servos:   0   1    2    3
          [ 60, 120, 213, 177],
          [240, 208, 110, 255],
          [240, 208, 100, 170],
          [ 60,  89, 233, 170],
          [ 60,  89, 233, 255],
         ]

logging=False
logging=True
servoPins = (15, 13, 12, 14)
servoInfo = [
  {'id':0, 'pin':15, 'min': 90, 'max':255},
  {'id':1, 'pin':13, 'min': 90, 'max':228},
  {'id':2, 'pin':12, 'min':110, 'max':244},
  {'id':3, 'pin':14, 'min':170, 'max':255},
]
speedPwmPin = Pin(0)
pwmFreq = 100 # On ESP all pins support pwm but the pins share the same freq!!
delayTicks = min(pwmFreq, 2)

class Servo():
  speedPwm = PWM(speedPwmPin, freq=pwmFreq, duty=1) # step delay for smooth movement
  def __init__(self, id, pin, min=55, max=255):
    self.id = id
    self.pin, self.min, self.max = (pin, min, max)
    mid = round((min+max)/2)
    self.midPos, self.position, self.target, self.step = (mid, mid, mid, 0)
    servoPwm = PWM(Pin(pin), freq=pwmFreq, duty=self.midPos)
    self.goto = servoPwm.duty  # pwm duty cycle determines position
    self.goto(mid)
    if logging: print(self)

  def __str__(self):
    return 'id={} pin={} min={} max={} mid={} pos={} tar={} step={}'.format(
      self.id, self.pin, self.min, self.max,
      self.midPos, self.position, self.target, self.step)

  def right(self, step=1):
    self.target += step
    self.prepare(self.target)
    return self.target

  def left(self, step=1):
    return self.right(-step)

  def mid(self):
    self.target = self.midPos
    self.prepare(self.target)
    return self.target

  def limit(self, pos):
    pos = max(self.min, pos)
    pos = min(self.max, pos)
    return pos

  def next(self):
    if self.position == self.target:
      return
    #if logging: print('====next: ', self)
    self.position = self.limit(self.position + self.step)
    self.jump()

  def jump(self, pos=-1):
    if pos == -1:
      pos = self.position
    if logging: print('====jump: ', self)
    self.position = pos
    self.goto(pos)

  def prepare(self, target):
    self.target = self.limit(target)
    steps = abs(self.target - self.position)
    if steps == 0:
      return 0
    self.step = 1 if self.target > self.position else -1
    if logging: print('====prepare: ', self)
    return steps

def servoStepGen():
  """
  interrupt handler for stepping the servos
  This handler is called pwmFreq times per second
  To lower the effective frequency we use a global counter named delayTicks in
  combination with a generator
  """
  for i in range(delayTicks):
    yield
  for servo in servos:
    servo.next()  # function to be called after delayTicks interrupts

servoStepIter = servoStepGen()
def servoStep(pin):
  global servoStepIter
  try:
    next(servoStepIter)
  except StopIteration:
    servoStepIter = servoStepGen() # start new delay

def run():
  points.append([servo.position for servo in servos])
  for positions in points:
    maxSteps = max(servo.prepare(target) for servo,target in zip(servos,positions))
    if logging: print('-------- sleeping 6 seconds -----------')
    sleep(6)
  print('done')


def r(id, step=1): servos[id].move(servos[id].position + step)
def l(id, step=1): r(id, -step)
def m(position): servos[id].move(position)
def mid():
  for id in range(len(servos)):
    servos[id].mid()

def doCmd(cmd):
  response = {'cmd':cmd}
  tup = cmds.get(cmd)
  if callable(tup):
    tup()
  elif not tup:
    if cmd!='Hello':
      print("command not found:" + cmd)
  else:
    id = tup[0]
    step = tup[1]
    #print('====step (%d,%d)' % (id, step))
    pos = servos[id].right(step)
    response.update({'id':id, 'pos':pos})
  return json.dumps(response)

cmds = {
  'run':run, 'midPos':mid,
   'Links':(0,+17), 'Vooruit':(1,+17), 'Omhoog':(2,+17), 'Pakken':(3,-17),
   'links':(0,+ 1), 'vooruit':(1,+ 1), 'omhoog':(2,+ 1), 'pakken':(3,- 1),
  'rechts':(0,- 1),   'terug':(1,- 1), 'omlaag':(2,- 1), 'lossen':(3,+ 1),
  'Rechts':(0,-17),   'Terug':(1,-17), 'Omlaag':(2,-17), 'Lossen':(3,+17),
}

servos = [Servo(**args) for args in servoInfo]

speedPwmPin.irq(trigger=Pin.IRQ_RISING, handler=servoStep)
#freq=Servo.speedPwm.freq
